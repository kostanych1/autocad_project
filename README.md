AutoCAD_project
==============================
    TLDR: Implementing Machine Learning and Python in the AutoCAD drawings

  Предназначено для ускорения работы с конструкторскими чертежами. Машинное обучение позволит убрать рутинные операции. Для работы нужен только чертёж автокада в текстовом формате DXF. Сделать его можно собственно автокадом из .dwg чертежа. На линуксе, к сожалению, автокад не работает, поэтому для полного цикла нужна ещё и винда.

  **Задачи:**
## **1. Обеспечить работу с AutoCAD на Python.**
  Автокад нативно не дружит с питоном. Для решения проблемы использовались COM-объекты и всякие win32 либы. Опыт негативный - далеко не все данные объектов можно достать таким образом. Например, в одном виде размеров появляется функция передачи координат на которых они стоят, на другом типе размеров такого нет. Поэтому используется преобразование чертежа в текстовой DXF файл, из которого в текстовом виде достаются нужные данные, так как там есть описание всех параметров всех объектов, и далее производится их обработка.

## **2. ML часть(в разработке)**
  Программа должна проставлять размеры на точки крепления к приборам, с учётом всех требований ЕСКД и располагая их в правильном положении относительно друг друга. С имеющейся базой можно расширить применение на другие задачи работы с dwg чертежами.

  Проект читает хорошие чертежи, обучается на существующих позициях размеров. Фичи - данные о линиях в слое с точками крепления, пересечение с линиями прибора, и т. д. Target - базовые точки размеров, положение текста размера

  "Демонстрационная" первая версия: будет один вид, нужно проставить все размеры на 1 виде.

  Работа тестируется на файле train.dwg. Предварительно нужно открыть его в автокаде, т.к. возможно читать только активный документ автокада.


### Условия:

  - ~~Размеры линейные, aligned dimensions.~~ Теперь поддерживаются любые типы размеров, да и вообще любые объекты
  - Один вид
  - Фиксированные значения длин линий точек привязки для размеров.
  - Строго определённые слои для разных объектов. Это требование должно выполняться и в итоговой версии, как и чистота и правильность всех чертежей.


### Решённые на данный момент задачи:
  - Чтение dwg файла средствами python, через com объекты.
  - Чтение данных всего чертежа (DXF-формат) и извлечение из него нужной информации.
  - Возможность взрывать блоки.
  - Извлечение координат точек крепления приборов, формирование датафрейма с данными для каждого прибора.
  - Формирование датафрейма с фичами размеров- координаты точек привязки и текста размера.



### Необходимо реализовать:

   - Линии приборов в датафрейме приборов
   - Модель машинного обучения по собранным координатам объектов

HOW TO USE
----------
1. Run from the root folder: ```dvc repro```
    That's all!
    




Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
