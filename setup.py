from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Implementing Machine Learning and Python in the AutoCAD drawings',
    author='Konstantin Sviblov',
    license='MIT',
)
