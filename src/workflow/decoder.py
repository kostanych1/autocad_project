"""Decode DXF file """

import click
import pandas as pd

from src import config
from src.data.files import open_dxf_file
from src.utilities import get_logger

logger = get_logger("Decoding")


# @click.command()
# @click.option(
#     "--output-path",
#     "-o",
#     default=config.DECODED_DIMENSIONS_DF,
#     help="Path to the output file",
# )
def decode_dimensions(output_path: str):  # noqa: C901
    """
    Function for the decoding codes of object parameters in the DXF file.
    For now, it uses hardcoded variables, it will be replaced by separated
    dictionary file in the future.
    Args:
        dxf: DXF file
        dimensions: index of the line in DXF file where dimension info starts.
        output_path: Output path for the dataframe with the decoded data

    Returns:
        DataFrame with the data about dimensions.
    """
    dxf = open_dxf_file()
    cols = [
        "start_X",
        "end_X",
        "start_Y",
        "end_Y",
        "text_X",
        "text_Y",
        "dim_line_X",
        "dim_line_Y",
    ]
    df = pd.DataFrame(columns=cols)
    dimensions = [
        9992,
        10050,
        10230,
        10328,
        10384,
        10482,
        10538,
        10912,
        10984,
        11054,
        11126,
        11198,
        11480,
    ]
    for ent in dimensions:
        # Extracting data from dxf
        data = []
        for pos, line in enumerate(dxf[ent:]):
            if line == " 13\n":
                data.append(float((dxf[ent + pos + 1]).rstrip()))
                # print(f'1st point X : {dxf[ent+pos+1]}')
            elif line == " 23\n":
                data.append(float((dxf[ent + pos + 1]).rstrip()))
                # print(f'1st point Y : {dxf[ent+pos+1]}')
            elif line == " 14\n":
                data.append(float((dxf[ent + pos + 1]).rstrip()))
                # print(f'2nd point X : {dxf[ent+pos+1]}')
            elif line == " 24\n":
                data.append(float((dxf[ent + pos + 1]).rstrip()))
                # print(f'2nd point Y: {dxf[ent+pos+1]}')
            elif line == " 11\n":
                data.append(float((dxf[ent + pos + 1]).rstrip()))
                # print(f'Text coord X: {dxf[ent+pos+1]}')
            elif line == " 21\n":
                data.append(float((dxf[ent + pos + 1]).rstrip()))
                # print(f'Text coord Y: {dxf[ent+pos+1]}')
            elif line == " 10\n":
                data.append(float((dxf[ent + pos + 1]).rstrip()))
                # print(f'Dimension line X: {dxf[ent+pos+1]}')
            elif line == " 20\n":
                data.append(float((dxf[ent + pos + 1]).rstrip()))
                # print(f'Dimension line Y: {dxf[ent+pos+1]}')

            elif (dxf[ent + pos] == "AcDbEntity\n") or (dxf[ent + pos] == "ENDSEC\n"):
                break
        current_col = pd.Series(
            data=data,
            index=cols,
        )
        df = pd.concat([df, current_col.to_frame().T], ignore_index=True)
    logger.info(f"df shape: {df.shape}")
    logger.info('Entities decoded!')
    # df.to_csv(config.DECODED_DIMENSIONS_DF, index=False)
    df.to_csv(output_path, index=False)
    logger.info('Dataframe saved to .csv!')
    # return df


if __name__ == "__main__":
    decode_dimensions()
