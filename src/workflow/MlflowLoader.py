import os

import mlflow
from src.utilities import get_logger

logger = get_logger('Mlflow Loader')


class MlflowLoader:
    """
    Class for conceiving operations with model.
    """

    def __init__(self):
        self.mlflow_artifact_path = "artifacts/model"
        self.experiment_id = None
        self.model_name = 'Unnamed_Model'
        self.model_uri = None
        self.pg_container_name = os.getenv("PG_CONTAINER")
        self.tracking_uri = f"postgresql://{os.getenv('PG_USER')}:{os.getenv('PG_PASS')}@{self.pg_container_name}/postgres"  # noqa: E501
        mlflow.set_tracking_uri(self.tracking_uri)
        self.experiment_name = 'Unnamed_Experiment'
        self.bucket = os.getenv("AWS_BUCKET")
        self.client = mlflow.MlflowClient()

    def prepare_experiment(self):
        """
        Set mlflow experiment, if there is no experiment with this name.
        """
        if not mlflow.get_experiment_by_name(self.experiment_name):
            mlflow.create_experiment(
                name=self.experiment_name, artifact_location=os.getenv("AWS_BUCKET")
            )
        mlflow.set_experiment(self.experiment_name)

    def train_classifier(self, model, X_train, y_train):
        """Train model"""
        # Train the model
        logger.debug(f"pg_container_name: {self.pg_container_name}")
        logger.debug(f"tracking_uri: {self.tracking_uri}")
        logger.info(f"experiment_name: {self.experiment_name}")
        logger.info(f"AWS bucket: {self.bucket}")
        model.fit(X_train, y_train)
        return model

    def set_model_stage(self, model_details):
        """
        Set stage of last model to 'Production'.
        Set stage of previous model to 'Archived'
        Parameters
        ----------
        model_details:
            Object with the model and mlflow run info
        """
        # Set stage of trained model to Production
        self.client.transition_model_version_stage(
            name=model_details.name,
            version=model_details.version,
            stage="Production",
        )

        # Set stage of previous model to 'archived'
        models = self.client.search_model_versions(f"name='{model_details.name}'")
        if len(models) > 1:
            previous_version = models[0].version - 1
            self.client.transition_model_version_stage(
                name=model_details.name,
                version=previous_version,
                stage="Archived",
            )

    def log_and_register_model(self, model):
        """
        Log and register model. Make some info for the logger
        Parameters
        ----------
        model:
            Trained model.

        Returns
        -------
        model_details:
            Object with the model and mlflow run info
        """
        run_id = mlflow.active_run().info.run_id
        artifact_path = self.mlflow_artifact_path
        model_uri = f"runs:/{run_id}/{artifact_path}"
        model_s3_path = f"/{run_id}/{artifact_path}/model.pkl"
        logger.info(f"Run id: {run_id}")
        logger.info(f"artifact_path: {artifact_path}")
        logger.info(f"model_uri: {model_uri}")
        logger.info(f"model_s3_path: {model_s3_path}")
        logger.info(f"Log model with name {self.model_name} and uri {model_uri}")

        mlflow.sklearn.log_model(model, "model")
        model_details = mlflow.register_model(model_uri=model_uri,
                                              name=self.model_name)
        logger.info("Model saved in run %s" % mlflow.active_run().info.run_uuid)
        logger.info("Model logged!")
        return model_details
