"""Train model"""
import os

import mlflow
import pandas as pd
from sklearn.multioutput import MultiOutputRegressor
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import numpy as np

from src.utilities import get_logger
from src.workflow.MlflowLoader import MlflowLoader

logger = get_logger("Main")


def model_training(df):
    """Train model"""
    with pd.option_context('display.max_rows', None,
                           'display.max_columns', None,
                           'display.precision', 3,
                           ):
        print(df)

    model_loader = MlflowLoader()
    model_loader.model_name = os.getenv("MLFLOW_MODEL_NAME")
    model_loader.experiment_name = os.getenv("MLFLOW_EXPERIMENT_NAME")

    # Start Mlflow run
    with mlflow.start_run(run_name=f"run_{model_loader.experiment_name}"):
        # Fit-predict
        target = ['text_X', 'text_Y']
        val = df.iloc[0]
        df1 = df.drop([0])
        y = df1[target]
        X = df1.drop(columns=target)
        val = val.T.drop(labels=target)

        # Train-test split
        X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=24)

        # We have 2 coordinates, so we will use multi-regressor
        multioutput = MultiOutputRegressor(LinearRegression()).fit(X_train, y_train)

        # Get preds
        preds = multioutput.predict(np.array(val).reshape(1, -1))
        score = multioutput.score(X_train, y_train)
        logger.info(f"score: {score}")
        logger.info(f"preds: {preds}")
        model_loader.log_and_register_model(multioutput)
        logger.info("Model logged!")
    return preds
