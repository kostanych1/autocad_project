"""Configuration holder """

import os
import sys

from pathlib import Path

sys.path.insert(0, os.getcwd())
PROJECT_DIR = Path(__file__).resolve().parent.parent
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = PROJECT_DIR / "data"
DATA_DIR = PROJECT_DIR / "data"
DECODED_DIMENSIONS_DF = str(DATA_DIR) + '/interim/decoded_df.csv'
