"""Main script """


from src.data import files
from src.data import handler
from src.utilities import get_logger
from src.models.train_model import model_training

logger = get_logger("Main")
dxf = files.open_dxf_file()
df, entities, dimensions = handler.get_dim_info(dxf)
preds = model_training(df)
# logger.info(df)
# logger.info(preds)
