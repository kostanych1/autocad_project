"""Handle data from DXF file """

import pandas as pd
from src.utilities import get_logger

from src import config
from src.workflow.decoder import decode_dimensions

logger = get_logger("Training")


def get_dim_info(dxf: list):
    """
    Extracts dimensions data from a dxf file.
    Args:
        dxf: list with data from DXF file

    Returns:
        None
    """
    # Looking for lines in the data, contained in the dxf file
    entities, dimensions = [], []
    for i, element in enumerate(dxf):
        # Number of entities in dxf file
        if element == "AcDbEntity\n":
            entities.append(i)
        # Number of dimensions in dxf file
        elif element == "AcDbDimension\n":
            dimensions.append(i + 1)
    print(f"Objects at the drawing: {len(entities)}\n Dimensions : {len(dimensions)}")
    decode_dimensions(config.DECODED_DIMENSIONS_DF)
    # decode_dimensions()
    df_dimensions = pd.read_csv(config.DECODED_DIMENSIONS_DF)
    logger.info('Dimensions decoded!')
    return df_dimensions, entities, dimensions


def find_attachment_point(data):
    """
    Converts given data to the coordinates of attachment points.
    Args:
        data: dataframe with coordinates of lines in layer 'attachment point'

    Returns:
        None
    """
    # FindAttachmentPoint(df[df['device']==device])
    print(f"data:\n{data}")
    print(data.iloc[[0]])


def make_dim_vertical(msp, point1, point2, loc):
    """
    Makes vertical dimension.
    :param msp: model space
    :param point1: point of dimension
    :param point2: point of dimension
    :param loc: location of dimension
    :return:
    """
    pass


def make_dim_horizontal(msp, point1, point2, loc):
    """
    Makes horizontal dimension.
    :param msp: model space
    :param point1: point of dimension
    :param point2: point of dimension
    :param loc: location of dimension
    :return:
    """
    pass


def process_attachment_points(msp, objects):
    """
    Makes coordinates of attachment points.
    :param msp: model space
    :param objects: lines from specified layer
    :return:
    """
    pass
