from pathlib import Path


def get_project_root() -> str:
    """Returns project root folder.
    Note:
        In any module in the current project get the project root as follows:
            from src.utils.utils import get_project_root
            root = get_project_root()
        Any module which calls get_project_root can be moved without
        changing program behavior.
    """
    return str(Path(__file__).parents[2])


def open_dxf_file(filename="testfile.DXF") -> list:
    """
    Opens DXF file by given name.
    :param filename: name of DXF file
    :return: list with all drawing data
    """
    dxf_file = open(get_project_root() + "/data/raw/" + filename, "r")
    dxf = list(dxf_file)
    dxf_file.close()

    # we can cut \n if we need
    # dxf = [st.rstrip() for st in dxf]
    return dxf


def make_dxf_file(activedoc, filename: str = "./testfile"):
    """
    Makes DXF file.
    :param activedoc:
    :param filename: name of DXF file
    :return: none
    """
    filename = filename
    sset = activedoc.SelectionSets.Add("SSET")
    activedoc.Export(filename, "DXF", sset)
